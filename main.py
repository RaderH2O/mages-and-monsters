#site : https://codeforces.com/contest/792/problem/F

#Spells characterized by 2 values : x[i] and y[i]
#Doesn't have to use spell for int(x) amounts of seconds
#x = damage , y = mana cost , z = seconds
#deals x * z damage spends y * z mana
#If no mana is left , canUseSpell = False
#Can fight monsters : t[j] and h[j] . t = time to kill vova's char , h = monster's hp
#After fight , mana is refilled
#if dealt h[j] damage no more than t[j] seconds using his spells , monster j dies and vova's mana refills (and if monster's health becomes exactly 0 in t[j] , the same thing happens)

#Program :
#Vova learns a new spell dealing x dps and y mps (mana per second(s) )
#Fights a monster which kills him in t seconds and has h hp
#(Vova doesn't know any spell at the beginning)

#What to do ?
#You have to determine if vova's able to win the fight or no

#Input :
#First line :
#q -> the number of queries , m -> amount of mana at the beginning of fight
#2 =< q <= 10 ** 5 , 1 <= m <= 10 ** 12
#then for i=1 in range(1, q) do this :
#input 3 int values : 1 <= k[i] <= 2 , 1 <= a[i] and b[i] <= 10 ** 6
#k is basically the action you want to do . either 1 or 2 . 1 means character learns a spell with x (dps) = (a[i] + j (the last monster you fought with) ) % (10 ** 6) + 1 <- for limiting it to be less than 10 ** 6
#and 2 means if character is able to win against the monster or no (output = "YES" or "NO") with time to kill of t = (a[i] + j) % (10 ** 6) + 1 and hp of h = (b[i] + j) % (10 ** 6) + 1

#inputs must be in input file in the same directory of this file

#Start of code :

x = []
y = []
killedMonster = False
j = 0
t, h = 0,0
q, m = 0, 0
def get_input():
    global q, m
    k, a, b = [],[],[]
    values = input()
    q, m = int(values.split(" ")[0]), int(values.split(" ")[1])
    for i in range(0,q):
        inp = input()
        k.append(int(inp.split(" ")[0]))
        a.append(int(inp.split(" ")[1]))
        b.append(int(inp.split(" ")[2]) % 10 ** 6)
    value = [k,a,b]
    return value

k, a, b = get_input()

for i in range(q):
    if k[i] == 1:
        x.append((a[i] + j) % 10 ** 6 + 1)
        y.append((b[i] + j) % 10 ** 6 + 1)
    elif k[i] == 2:
        t = (a[i] + j) % 10 ** 6 + 1
        h = (b[i] + j) % 10 ** 6 + 1
        j += 1
        for chosen in range(0,len(x)):
            if x[chosen] * t >= h and y[chosen] * t <= m * 2:
                print("YES")
                killedMonster = True
                break
        if not killedMonster:
            print("NO")

        else:
            killedMonster = False
